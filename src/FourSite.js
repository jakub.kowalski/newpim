import React, { Component } from 'react';
import logo from './logo-new-edited.png';
import './App.css';
import pim from './show_fields.json';
import { Redirect } from 'react-router-dom';

function parseStations(data) {
  return data.map((result) => {

    return  {label : result.asset[0]["External Id"], img : result.asset[0]["A-AssetThumbnailURL"], container: result.asset[0]["Container"]}
    }
  );
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      data: [],
      counter:0,
     
    }
  }
  componentDidMount() {
    const products = pim.entity;
    this.setState({
      data: parseStations(products),
    })
  }
 
  changepage=(e)=>{
    console.log(e.target.id);
    let reference = e.target.id;
    this.setState({
        fireRedirect: true,
        pathname: reference,
    })
      }
  render(
  ) {
    return (


        <div className="shopping-cart">
        <div className="title">
          Działa 4 strona
         </div>

     <div className="pager">
      
      <input type="submit" value="1" id="/" onClick={this.changepage}/>
      <input type="submit" value="2" id="page2"onClick={this.changepage}/>
      <input type="submit" value="3" id="page3"onClick={this.changepage}/>
      <input type="submit" value="4" id="page4"onClick={this.changepage}/>

      </div>
      {this.state.fireRedirect && < Redirect to={{
          pathname:this.state.pathname,
          
        }} />}
      </div>




    );
  }
}

export default App;
