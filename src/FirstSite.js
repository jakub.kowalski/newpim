import React, { Component } from 'react';
import logo from './logo-new-edited.png';
import './App.css';
import pim from './show_fields.json';
import { Redirect } from 'react-router-dom';
import Pagination from "react-js-pagination";


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            itemPerPage: 12,
            fireRedirect: false,
            pathname: "/",
            activePage: 1,
            projectList: [],
            originalProjectList: [],
            searchText: '',
            isdiagram: true,
            isActive: false,
            renders:0,
          
        }


    }
    componentDidMount() {

        const products = pim.entity;
        let data = products.map((result) => {
            return { label: result.asset[0]["External Id"], img: result.asset[0]["A-AssetThumbnailURL"], container: result.asset[0]["Container"] }
        })

        this.setState({

            projectDetails: data,
            originalProjectList: data,

        })
    }
    handlePageChange(pageNumber) {
        console.log(`active page is ${pageNumber}`);
        this.setState({ activePage: pageNumber });

    }

    changeQuantity = (e) => {
        let change = e.target.value;
        if (change == 36 || change == 24) {
            this.setState({
                itemPerPage: change,
                activePage: 1,
            })
        }

        else {
            this.setState({
                itemPerPage: change
            })
        }
    }
    updateInputValue = (e) => {
        this.setState({
            searchText: e.target.value,
            isActive: true,
        }, function () {
            let textToSearch = this.state.searchText;
            let originalData = this.state.projectList;

            if (textToSearch !== undefined || textToSearch != '') {
                let searchData = [];

                for (var i = 0; i < this.state.projectList.length; i++) {
                    if (this.state.projectList[i].label.indexOf(textToSearch) != -1 || this.state.projectList[i].container.indexOf(textToSearch) != -1) {
                        searchData.push(this.state.projectList[i]);
                        console.log(searchData.length)
                        
                        
                    }
                }
                this.setState({
                    projectList: searchData
                });
            }
            if (textToSearch == '') {
                this.setState({
                    projectList: this.state.originalProjectList,
                });
            }
        });
    }
change=(e)=>{
//     let counter = this.state.projectList.length
// this.setState({
//     originalProjectList: counter
// })
}

    render(
    ) {
        var indexOfLastTodo = this.state.activePage * this.state.itemPerPage;
        var indexOfFirstTodo = indexOfLastTodo - this.state.itemPerPage;
        var renderedProjects = this.state.originalProjectList.slice(indexOfFirstTodo, indexOfLastTodo);
        var renders = this.state.projectList.slice(indexOfFirstTodo, indexOfLastTodo);
        if (this.state.isActive === true) {
         
            
            var item = renders.map((item, key) => {
                return <tr key={key}>
                    <td className="buttons">
                        <span className="delete-btn"></span>
                        <span className="like-btn"></span>
                    </td>
                    <td className="image">
                        <img src={item['img']} alt="" />
                    </td>
                    <td className="description">
                        <span>
                            {item['label']}
                        </span>
                    </td>
                </tr>

});
this.change()


        }
        else {
            var listItems = renderedProjects.map((item, index) => {
                return <tr key={index}>
                    <td className="buttons">
                        <span className="delete-btn"></span>
                        <span className="like-btn"></span>
                    </td>
                    <td className="image">
                        <img src={item['img']} alt="" />
                    </td>
                    <td className="description">
                        <span>
                            {item['label']}
                        </span>
                        <span>
                            {item['container']}
                        </span>
                    </td>
                    <td className="quantity">
                        <input type="submit" value="+" onClick={this.increment} />
                        <input onClick={this.decrement} type="submit" value="-" />

                    </td>


                </tr>
            })
        }
        return (
            <div className="container">
                <div className="shopping-cart">
                    <div className="title .text-center">
                        Shopping Bag
                    </div>
                    <div className="top-menu">
                        <div className="inputBox">
                            <input type="text" className="form-control" placeholder="Search" value={this.state.searchText} onChange={this.updateInputValue} />
                        </div>
                        <div className="select">
                            <select onChange={this.changeQuantity}>
                                <option value="12" >12</option>
                                <option value="24" >24</option>
                                <option value="36" >36</option>
                            </select>


                        </div>
                    </div>
                    <div className="panel-body">
                        <table className="table table-striped">

                            <tbody>
                                {listItems}
                                {item}
                            </tbody>
                        </table>
                    </div>

                    <div className="pagination">

                        <Pagination
                            activePage={this.state.activePage}
                            itemsCountPerPage={this.state.itemPerPage}
                            totalItemsCount={this.state.originalProjectList.length}
                            pageRangeDisplayed={10}
                            onChange={this.handlePageChange.bind(this)}
                        />
                    </div>


                </div>
            </div>

        );
    }
}

export default App;
