import React, { Component } from 'react';
import logo from './logo-new-edited.png';
import './App.css';
import pim from './show_fields.json';
import { Redirect } from 'react-router-dom';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = { 
      data: [],
      dataRemove: [],
     
    }
  }
  componentDidMount() {
    let data = this.props.location.state.otherProducts;
    
    let datadelete = data.splice(0,12);
    this.setState({
        data:data,
        dataRemove:datadelete,
    })
  }
  changepage=(e)=>{
    console.log(e.target.id);
    let reference = e.target.id;
    this.setState({
        fireRedirect: true,
        pathname: reference,
    })
      }
  render(
  ) {
    return (

<div className="shopping-cart">
        <div className="title">
           3 stronka
 </div>
        <div className="select">
            <select onChange={this.changeQuantity}>
                <option value="12" >12</option>
                <option value="24" >24</option>
                <option value="36" >36</option>
            </select>
        </div>
        {this.state.dataRemove.map((result, index) => (

            <div className="item" key={index} >
                <div className="buttons">
                    <span className="delete-btn"></span>
                    <span className="like-btn"></span>
                </div>
                <div className="image">
                    <img src={result['img']} alt="" />
                </div>
                <div className="description">

                    <span>
                        {result['label']}
                    </span>
                    <span>
                        {result['container']}
                    </span>
                </div>
                <div className="quantity">
                    <input type="submit" value="+" onClick={this.increment} id={index} />
                    <output>{this.state.counter}</output>
                    <input onClick={this.decrement} id={index} type="submit" value="-" />

                </div>

            </div>
        ))}

     <div className="pager">
      
      <input type="submit" value="1" id="/" onClick={this.changepage}/>
      <input type="submit" value="2" id="page2"onClick={this.changepage}/>
      <input type="submit" value="3" id="page3"onClick={this.changepage}/>
      <input type="submit" value="4" id="page4"onClick={this.changepage}/>

      </div>
      {this.state.fireRedirect && < Redirect to={{
          pathname:this.state.pathname,
          
        }} />}
      </div>


    );
  }
}

export default App;
